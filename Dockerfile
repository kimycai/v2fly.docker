FROM ubuntu:20.04
RUN apt-get update && apt-get -y upgrade
RUN apt-get -y install curl && apt-get -y install sudo && apt-get -y install systemctl && apt-get -y install systemd
RUN apt-get -y install nano && apt-get -y install vim && apt-get -y install wget

RUN wget https://raw.githubusercontent.com/v2fly/fhs-install-v2ray/master/install-release.sh 
RUN sudo bash install-release.sh 
RUN rm install-release.sh
